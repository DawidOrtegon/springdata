package data.springip2;

import data.springip2.Security.PasswordEncoderConfig;
import data.springip2.User.User;
import data.springip2.User.UserDto;
import data.springip2.User.UserDtoBuilder;
import data.springip2.User.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

// Puts the information about the users in the database with the encrypted password.
@Component
public class UserActivation {
    private UserManager userManager;
    private UserDtoBuilder userDtoBuilder;

    // Constructor.
    @Autowired
    public UserActivation(UserManager userManager, UserDtoBuilder userDtoBuilder) {
        this.userManager = userManager;
        this.userDtoBuilder = userDtoBuilder;
    }

    // Empty Constructor
    public UserActivation() {
    }

    // Method that includes the data inside the database.
    @EventListener(ApplicationReadyEvent.class)
    public void saveUserDB()
    {
        // Solamente la clave para el ADMIN Esta asegurada, la del usuario normal no, aunque ambas deben estar asi en la base de datos.
        User user1 = userDtoBuilder.setUser(new UserDto("user1","user1abc","ROLE_CUSTOMER"));
        userManager.addUser(user1);
        User user2 = userDtoBuilder.setUser(new UserDto("user2","user2abc","ROLE_ADMIN"));
        userManager.addUser(user2);
    }
}
