package data.springip2;

import data.springip2.manager.CustomerManager;
import data.springip2.manager.ProductManager;
import data.springip2.persistance.entities.Customer;
import data.springip2.persistance.entities.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

// Endpoint donde se especifica a cuales métodos tiene derecho el usuario tipo cliente.
@RestController
@RequestMapping("api/user")
public class UserControllerApi
{
    private ProductManager productManager;
    private CustomerManager customerManager;

    @Autowired
    public UserControllerApi(ProductManager productManager, CustomerManager customerManager) {
        this.productManager = productManager;
        this.customerManager = customerManager;
    }

    // Retorna el producto solo con el ID.
    @GetMapping(value = "/product")
    public Optional<Product> getProductById(@RequestParam Long index) {
        return productManager.findById(index);
    }


    // Para los CLIENTES
    // Da el cliente buscado por id.
    @GetMapping(value = "/customer")
    public Optional<Customer> getCustomerById(@RequestParam Long index)
    {
        return customerManager.findById(index);
    }

    // Da todos los clientes.
    @GetMapping(value = "/customer/all")
    public Iterable<Customer> getCustomerAll()
    {
        return customerManager.findAll();
    }

}
