package data.springip2.persistance;

import data.springip2.persistance.entities.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

// Class that has all the information of the database, is the bridge.
@Repository
public interface ProductRepo extends CrudRepository<Product,Long>
{

}
