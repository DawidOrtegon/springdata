package data.springip2.persistance.entities;

import javax.persistence.*;

@Entity
@Table(name = "TheCustomer")
public class Customer
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long Id;

    // Attributes
    private String name;
    private String address;

    // Constructor without Arguments
    public Customer()
    {
    }

    // Constructor with arguments.
    public Customer(String name, String address) {
        this.name = name;
        this.address = address;
    }

    // Getter and Setter Methods
    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
