package data.springip2.persistance.entities;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "TheOrder")
public class Order
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    // Attributes.
    @ManyToOne
            (
                cascade = {
                        CascadeType.ALL,
                }
            )
    private Customer customer;

    @ManyToMany
            (
                    cascade = {
                            CascadeType.ALL,
                    }
            )
    private Set<Product> products = new HashSet<>();
    private LocalDateTime date;
    private String status;

    //Constructor without arguments.
    public Order()
    {
    }

    // Constructor without the ID
    public Order(Customer customer, Set<Product> products, LocalDateTime date, String status) {
        this.customer = customer;
        this.products = products;
        this.date = date;
        this.status = status;
    }


    //Getter and setter Methods.
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
