package data.springip2.User;

import data.springip2.Security.PasswordEncoderConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserDtoBuilder
{
    @Autowired
    private PasswordEncoderConfig passwordEncoderConfig;

    public User setUser(UserDto userDto)
    {
        return new User(userDto.getName(), passwordEncoderConfig.passwordEncoder().encode(userDto.getPassword()), userDto.getRole());
    }

}
