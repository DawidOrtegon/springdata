package data.springip2.User;

// Just a class to have the User defined, but not an entity.
import javax.persistence.*;

@Entity
@Table(name = "Users")
public class User
{    // Attributes

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    private String passwordHash;
    private String role;

    // Empty constructor.
    public User()
    {

    }

    // Constructor with parameters.
    public User(String name, String passwordHash, String role)
    {
        this.name = name;
        this.passwordHash = passwordHash;
        this.role = role;
    }

    // Constructor with the id.
    public User(long id, String name, String passwordHash, String role) {
        this.id = id;
        this.name = name;
        this.passwordHash = passwordHash;
        this.role = role;
    }

    // Getter and setter methods.

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
