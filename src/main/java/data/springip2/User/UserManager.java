package data.springip2.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserManager
{
    // Has the method to only find the user by the name.
    @Autowired
    private UserRepository userRepository;

    public UserManager(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserManager() {
    }

    public User getUserByName (String name)
    {
        Optional<User> user = userRepository.findByName(name);

        // Si esta presente retorne el usuario, de lo contrario retorne null.
        return user.orElse(null);
    }

    public void addUser(User user)
    {
        userRepository.save(user);
    }

}
