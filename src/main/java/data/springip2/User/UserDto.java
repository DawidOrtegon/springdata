package data.springip2.User;

import javax.persistence.*;

// Guarda la clave cifrada del usuario.
public class UserDto
{
    private String name;
    private String password;
    private String role;

    // Empty Constructor
    public UserDto() {
    }

    // Constructor with Attributes.
    public UserDto(String name, String password, String role) {
        this.name = name;
        this.password = password;
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String passwordHash) {
        this.password = passwordHash;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
