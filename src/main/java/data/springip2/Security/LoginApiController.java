package data.springip2.Security;

import data.springip2.User.User;
import data.springip2.User.UserDto;
import data.springip2.User.UserDtoBuilder;
import data.springip2.User.UserManager;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class LoginApiController
{
    private PasswordEncoderConfig passwordEncoderConfig;
    private PasswordEncoder passwordEncoder;
    private UserManager userManager;
    private UserDtoBuilder userDtoBuilder;

    @Autowired
    public LoginApiController(PasswordEncoderConfig passwordEncoderConfig, PasswordEncoder passwordEncoder, UserManager userManager, UserDtoBuilder userDtoBuilder) {
        this.passwordEncoderConfig = passwordEncoderConfig;
        this.passwordEncoder = passwordEncoder;
        this.userManager = userManager;
        this.userDtoBuilder = userDtoBuilder;
    }


    @PostMapping("api/getToken")
    public String tokenGeneration(@RequestBody UserDto userDto)
    {
        User userFic = userDtoBuilder.setUser(userDto);
        User userRegistered = userManager.getUserByName(userDto.getName());

        // Verificar si el usuario esta dentro de la base de datos.
        if(userRegistered == null)
        {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build().toString();
        }

        //Verifique que la contraseña codificada obtenida del almacenamiento coincida con la contraseña sin procesar enviada después de que también esté codificada.
        if(!passwordEncoderConfig.passwordEncoder().matches(userDto.getPassword(),userRegistered.getPasswordHash()))
        {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build().toString();
        }

        long currentTimeMillis = System.currentTimeMillis();
        return Jwts.builder()
                .setSubject(userDto.getName())
                .claim("role",userRegistered.getRole())
                .setIssuedAt(new Date(currentTimeMillis))
                .setExpiration(new Date(currentTimeMillis + 90000))
                .signWith(SignatureAlgorithm.HS512,"felipeBernal")
                .compact();
    }
}
