package data.springip2.Security;

import data.springip2.Security.JwtFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

// Generation for the tokens according to the Role given to the user and validated here.
// Class where all the API security is specified.
@Configuration
@EnableWebSecurity
public class ApiSecurityConfig extends WebSecurityConfigurerAdapter
{
    // Seguridad para cuando se entra desde el navegador.
    @Override
    public void configure(WebSecurity web) throws Exception
    {
        // Ignore estas paginas cuando se entra desde el navegador, el api/customer no esta por seguridad.
        web.ignoring().antMatchers("/h2/**", "/api/getToken", "/api/products/**", "/api/orders/**", "/api/customers/**");
    }

    // Seguridad para cuando se entra desde Postman.
    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        http.authorizeRequests()
                .antMatchers("/api/user/**").hasRole("CUSTOMER")
                .antMatchers("/api/admin/**").hasRole("ADMIN")
                .antMatchers("/h2/**").permitAll()
                .anyRequest().authenticated().and()
                .httpBasic().and().csrf().disable().headers().frameOptions().disable()
                .and()
                .addFilter(new JwtFilter(authenticationManager()));
    }
}
