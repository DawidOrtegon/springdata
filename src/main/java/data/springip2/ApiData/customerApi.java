package data.springip2.ApiData;

import data.springip2.manager.CustomerManager;
import data.springip2.persistance.entities.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("api/customers")
public class customerApi
{
    private CustomerManager customerManager;

    @Autowired
    public customerApi(CustomerManager customerManager)
    {
        this.customerManager = customerManager;
    }

    // GET all the customers
    @GetMapping("/allCustomers")
    public Iterable<Customer> getCustomerAll()
    {
        return customerManager.findAll();
    }

    // GET some customer by the ID.
    @GetMapping
    public Optional<Customer> getCustomerById(@RequestParam Long index)
    {
        return customerManager.findById(index);
    }

    // POST a new customer.
    @PostMapping
    public Customer addCustomer(@RequestBody Customer customer)
    {
        return customerManager.save(customer);
    }

    // PUT a new customer with ID
    @PutMapping
    public Customer updateCustomer(@RequestBody Customer customer)
    {
        return customerManager.save(customer);
    }

    // Delete customer by id.
    @DeleteMapping
    public void deleteCustomerById(@RequestParam Long index)
    {
        customerManager.deleteById(index);
    }

}
