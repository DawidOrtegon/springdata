package data.springip2.ApiData;

import data.springip2.manager.OrderManager;
import data.springip2.persistance.entities.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("api/orders")
public class orderApi
{
    private OrderManager orderManager;

    @Autowired
    public orderApi(OrderManager orderManager)
    {
        this.orderManager = orderManager;
    }

    // GET all the Order in the API.
    @GetMapping("/allOrders")
    public Iterable<Order> getOrderAll()
    {
        return orderManager.findAll();
    }

    // GET some order by its ID.
    @GetMapping
    public Optional<Order> getOrderById( @RequestParam Long index)
    {
        return orderManager.findById(index);
    }

    // POST some new order.
    @PostMapping
    public Order addOrder(@RequestBody Order order)
    {
        return orderManager.save(order);
    }

    // PUT a new order.
    @PutMapping
    public Order updateOrder(@RequestBody Order order)
    {
        return orderManager.save(order);
    }

    // DELETE an order.
    @DeleteMapping
    public void deleteOrderById(@RequestParam Long index)
    {
        orderManager.deleteById(index);
    }
}
