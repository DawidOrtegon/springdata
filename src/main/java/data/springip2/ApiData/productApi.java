package data.springip2.ApiData;

import data.springip2.manager.ProductManager;
import data.springip2.persistance.entities.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/products")
public class productApi {
    private ProductManager productManager;

    @Autowired
    public productApi(ProductManager productManager) {
        this.productManager = productManager;
    }

    // GET all the products.
    @GetMapping("/allProducts")
    public Iterable<Product> getProductsAll() {
        return productManager.findAll();
    }

    // GET the product by ID.
    @GetMapping
    public Optional<Product> getProductById(@RequestParam Long index) {
        return productManager.findById(index);
    }

    // POST some new product in the table.
    @PostMapping
    public Product addProduct(@RequestBody Product product) {
        return productManager.save(product);
    }

    // Put some new product with an specified ID.
    @PutMapping
    public Product updateProduct(@RequestBody Product product)
    {
        return productManager.save(product);
    }

    // Delete a product by his ID.
    @DeleteMapping
    public void deleteProductById(@RequestParam Long index)
    {
        productManager.deleteById(index);
    }

    @PatchMapping
    public Product updatePartOfProduct(@RequestBody Product product)
    {
        return productManager.save(product);
    }

}
