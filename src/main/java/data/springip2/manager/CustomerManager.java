package data.springip2.manager;

import data.springip2.persistance.CustomerRepository;
import data.springip2.persistance.entities.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Optional;

@Service
public class CustomerManager
{
    private CustomerRepository customerRepoManager;

    @Autowired
    public CustomerManager(CustomerRepository customerRepoManager)
    {
        this.customerRepoManager = customerRepoManager;
    }

    // To return all the customers.
    public Iterable<Customer> findAll()
    {
        return customerRepoManager.findAll();
    }

    // To find some product by Id.
    public Optional<Customer> findById(Long index)
    {
        return customerRepoManager.findById(index);
    }

    // To add and save some customer to the database.
    public Customer save(Customer customer)
    {
        return customerRepoManager.save(customer);
    }

    // Delete a customer in the database.
    public void deleteById(Long index)
    {
        customerRepoManager.deleteById(index);
    }

    // Fill the table of customers.
    @EventListener(ApplicationReadyEvent.class)
    public void fillCustomerTable()
    {
        save(new Customer("Jak Kowalski", "Wroclaw"));
        save(new Customer("Konrad Kalbarczyk", "Szczecin"));
        save(new Customer("Janis Morales", "Bogota"));
        save(new Customer("Santiago Rueda", "Cali"));
        save(new Customer("Nicolas Parra", "Buenos Aires"));
        save(new Customer("Anderson Navarrete", "Medellin"));
    }

}
