package data.springip2.manager;

import data.springip2.persistance.OrderRepository;
import data.springip2.persistance.entities.Customer;
import data.springip2.persistance.entities.Order;
import data.springip2.persistance.entities.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class OrderManager
{
    private OrderRepository orderRepoManager;

    @Autowired
    public OrderManager(OrderRepository orderRepoManager)
    {
        this.orderRepoManager = orderRepoManager;
    }

    // Get all the orders.
    public Iterable<Order> findAll()
    {
        return orderRepoManager.findAll();
    }

    // Get some order by ID.
    public Optional<Order> findById(Long index)
    {
        return orderRepoManager.findById(index);
    }

    // Submit a new order.
    public Order save(Order order)
    {
        return orderRepoManager.save(order);
    }

    // Delete an order by ID
    public void deleteById(Long index)
    {
        orderRepoManager.deleteById(index);
    }

    // Add new Orders to the table
    Product product1 = new Product("Rura", 5f, true);
    Product product = new Product("Korek", 2.55f, true);
    Set<Product> products = new HashSet<>()
    {
        {
            add(product);
            add(product1);
        }
    };
    Customer customer = new Customer("Jak Kowalski", "Wroclaw");
    String orderStatus = "In progress";


    @EventListener(ApplicationReadyEvent.class)
    public void fillOrdersTable()
    {
        save(new Order(customer, products,LocalDateTime.now(),orderStatus));
    }
}
