package data.springip2.manager;

import data.springip2.persistance.ProductRepo;
import data.springip2.persistance.entities.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Optional;

@Service
public class ProductManager
{
    private ProductRepo productRepoManager;

    @Autowired
    public ProductManager(ProductRepo productRepoManager)
    {
        this.productRepoManager = productRepoManager;
    }

    // To find all the products.
    public Iterable<Product> findAll()
    {
        return productRepoManager.findAll();
    }

    // To find some product by ID.
    public Optional<Product> findById(Long index)
    {
        return productRepoManager.findById(index);
    }

    // Save new product inside the table.
    public Product save(Product product)
    {
        return productRepoManager.save(product);
    }

    // Delete any product with its id.
    public void deleteById(Long index)
    {
        productRepoManager.deleteById(index);
    }

    // Add the products to the default database.
    @EventListener(ApplicationReadyEvent.class)
    public void fillProductsTable()
    {
        save(new Product("Milo", 12f, false));
        save(new Product("Milk", 4f, true));
        save(new Product("Chocolate", 11f, true));
        save(new Product("Rice", 3f, true));
        save(new Product("Butter", 8f, true));
        save(new Product("Chips", 5f, true));
        save(new Product("Cheese", 11f, true));
    }
}
