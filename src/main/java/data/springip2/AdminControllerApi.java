package data.springip2;

import data.springip2.manager.CustomerManager;
import data.springip2.manager.OrderManager;
import data.springip2.manager.ProductManager;
import data.springip2.persistance.entities.Customer;
import data.springip2.persistance.entities.Order;
import data.springip2.persistance.entities.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("api/admin")
public class AdminControllerApi
{
    private ProductManager productManager;
    private CustomerManager customerManager;
    private OrderManager orderManager;

    @Autowired
    public AdminControllerApi(ProductManager productManager, CustomerManager customerManager, OrderManager orderManager) {
        this.productManager = productManager;
        this.customerManager = customerManager;
        this.orderManager = orderManager;
    }



    // FOR THE PRODUCTS
    // Pone un nuevo producto dentro de la base de datos.
    @PostMapping(value = "/product")
    public Product addProduct(@RequestBody Product product) {
        return productManager.save(product);
    }

    // Modifica el producto de acuerdo al id entrante.
    // http://localhost:8080/api/admin/product/10 se da la orden de esta forma para que lo lea.
    @PutMapping(value = "/product/{index}")
    public Product updateProductById(@RequestBody Product product, @PathVariable String index)
    {
        return productManager.save(product);
    }

    // Modifica alguna parte del producto dado.
    @PatchMapping(value = "/product")
    public Product updatePartOfProduct(@RequestBody Product product)
    {
        return productManager.save(product);
    }



    // FOR THE CUSTOMERS
    // Agrega un nuevo cliente a la base de datos.
    @PostMapping(value = "/customers")
    public Customer addCustomer(@RequestBody Customer customer)
    {
        return customerManager.save(customer);
    }

    // Modifica el usuario de acuerdo al id dado.
    @PutMapping(value = "/customers/{index}")
    public Customer updateCustomerByIb(@RequestBody Customer customer, @PathVariable String index)
    {
        return customerManager.save(customer);
    }

    // Modifica alguna parte del cliente dado.
    @PatchMapping(value = "/customers")
    public Customer updatePartOfCustomer(@RequestBody Customer customer)
    {
        return customerManager.save(customer);
    }



    // FOR THE ORDERS
    // Modifica una de las ordenes con el Id dado.
    @PutMapping(value = "/orders/{index}")
    public Order updateOrderByIb(@RequestBody Order order, @PathVariable String index)
    {
        return orderManager.save(order);
    }

    // Modifica alguna parte de la orden dada.
    @PatchMapping(value = "/orders")
    public Order updatePartOfOrder(@RequestBody Order order)
    {
        return orderManager.save(order);
    }

    // For the CUSTOMERS
    // Da el cliente buscado por id.
    @GetMapping(value = "/customer")
    public Optional<Customer> getCustomerById(@RequestParam Long index)
    {
        return customerManager.findById(index);
    }

    // Da todos los clientes.
    @GetMapping(value = "/customer/all")
    public Iterable<Customer> getCustomerAll()
    {
        return customerManager.findAll();
    }

}
